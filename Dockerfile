FROM nvidia/cuda:10.1-cudnn8-runtime

ENV USERNAME=user

RUN apt-get update
RUN apt-get -y upgrade

RUN apt-get -y install --no-install-recommends \
    wget \
    git \
    vim \
    curl \
    make \
    cmake \
    vim \
    sudo \
    build-essential \ 
    pkg-config \
    libgoogle-perftools-dev \
    language-pack-ja-base \
    language-pack-ja

RUN apt-get -y install zlib1g zlib1g-dev libjpeg-dev

RUN curl -sL https://deb.nodesource.com/setup_12.x |bash - \
&& apt-get install -y nodejs

RUN localedef -f UTF-8 -i ja_JP ja_JP.UTF-8

ENV LANG=ja_JP.UTF-8
ENV LANGUAGE=ja_JP:ja
ENV LC_ALL=ja_JP.UTF-8


RUN apt-get -y install python3
RUN apt-get -y install python3-pip

RUN pip3 install --upgrade pip

RUN pip3 install jupyter click numpy matplotlib seaborn pandas tqdm sklearn ipywidgets nltk
RUN pip3 install torch transformers sentencepiece
RUN pip3 install tornado==4.5.3

# install jupyter-kite
#RUN cd && \
#    wget https://linux.kite.com/dls/linux/current && \
#    chmod 777 current && \
#    sed -i 's/"--no-launch"//g' current > /dev/null && \
#    ./current --install ./kite-installer

# install jupyterlab & extentions
RUN pip3 install --upgrade --no-cache-dir \
    'jupyterlab~=3.0' \
#    'jupyterlab-kite>=2.0.2' \
    jupyterlab_code_formatter \
    jupyterlab-vimrc \
    yapf \
    && rm -rf ~/.cache/pip \
    && jupyter labextension install \
        @hokyjack/jupyterlab-monokai-plus \
        @ryantam626/jupyterlab_code_formatter \
        @axlair/jupyterlab_vim \
#        @kiteco/jupyterlab-kite \
    && jupyter serverextension enable --py jupyterlab_code_formatter

RUN pip3 install jedi==0.17.2

RUN pip3 install jedi==0.17.2
RUN useradd -m -s /bin/bash ${USERNAME}
USER ${USERNAME}

# Jupyter configuration
RUN jupyter notebook --generate-config
RUN sed -i.back \
    -e "s:^#c.NotebookApp.token = .*$:c.NotebookApp.token = u'':" \
    -e "s:^#c.NotebookApp.ip = .*$:c.NotebookApp.ip = '*':" \
    -e "s:^#c.NotebookApp.open_browser = .*$:c.NotebookApp.open_browser = False:" \
    /home/${USERNAME}/.jupyter/jupyter_notebook_config.py

WORKDIR /home/${USERNAME}
